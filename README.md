# arcade-angular

This project contains an Angular arcade app where we can build various games for users to play!

## Getting Started

### Prerequsites

1. Install [Git](https://git-scm.com/book/en/v2/Getting-Started-Installing-Git)
2. Install [Node (and NPM)](https://nodejs.org/en/)

### Clone the Repository

```
git clone https://bitbucket.org/pcallen1015/angular-interview-version-a.git
```

### Install Project Dependencies

```
cd angular-interview-version-a/
npm install
```

### Run the App

Start the Angular app

```
npm start
```

The server should start running at `localhost:4200`

---

# The Interview Exercise

## The Objective

The purpose of this exercise is for you to demonstrate your skills working with Angular. The project uses many of the technologies we frequently use to build applications in our team.

## The One Big Rule

**Your work should be your own**

You are welcome to use the resources that would normally be at your disposal (Google, Stackoverflow, Documentation, etc.), but your submission should reflect your own problem solving ability.

DON'T, for example, find an existing solution (if one exists), copy+paste, and submit that as your own work, thinking we won't notice. **We will**.

## The Project Structure

The structure of this project is based on Angular-CLI. If you've used this tool before, the structure should be familiar.

There's additional functionality built into the application. **For the sake of simplicity, we'll focus on what you'll be working on:**

```
src/
  |-- app/
    |-- games/
      |-- rock-paper-scissors/
        |-- rock-paper-scissors-game/
          |-- rock-paper-scissors-game.component.html
          |-- rock-paper-scissors-game.component.scss
          |-- rock-paper-scissors-game.component.spec.ts
          |-- rock-paper-scissors-game.component.ts
  |-- assets/
    |-- sass/
      |-- buttons.scss
      |-- variables.scss

```

#### GamesModule (src/app/games)

Inside `AppModule`, there's a submodule called `GamesModule`. This module houses some components and services useful to all games in our arcade, as well as the game implementations themselves (e.g.: Rock-Paper-Scissors).

#### RockPaperScissorsModule (src/app/games/rock-paper-scissors)

`RockPaperScissorsModule` is where we'll store our implementation for Rock-Paper-Scissors. Any components, services, classes, etc. related to Rock-Paper-Scissors should be stored here.

### Assets (src/assets)

In the `assets` directory, you'll find some styling already written that may be useful.

## Tips

1. Documentation - Everybody likes well-documentation code, and it helps understand your thought process.
2. Style and Visuals - We're not imposing specific requirements around visual style. We want to see what you can come up with!
3. Don't be afraid to go "above and beyond." If you have an idea for a cool additional feature, or an improvement to an existing one, or an alternatice design approach, GO FOR IT (once you've completed the required stuff, that is).

---
# THE EXERCISE REQUIREMENTS

## 1. The Game: Rock-Paper-Scissors

In `src/app/games/rock-paper-scissors`, implement a game of Rock-Paper-Scissors where a human player (you) plays against a simulated computer opponent. Here's how it should work...

### 1a. The "Start" Screen

The user should first be prompted to start the game. Nothing fancy here, just give the user a way to start the game.

![Start Screen](./src/assets/images/rock-paper-scissors/screen_1.png)

### 1b. Player Move Selection

Once the game is started, **the user will have 3 seconds** to select a move. During that time, the opponent's move is determined as well (randomly).

![Move Screen](./src/assets/images/rock-paper-scissors/screen_2.png)

**If the user fails to make a move in the time allotted, the game should still carry on as usual**

### 1c. The Outcome

Once the 3 seconds are up, the game is over and the result should be determined.

Win States:

- Rock beats Scissors
- Scissors beats Paper
- Paper beats Rock
- Anything beats Nothing

Display who won, and prompt the user to play again.

![Outcome Screen](./src/assets/images/rock-paper-scissors/screen_3.png)

## 2. Tracking Wins

Once we have the game working, it would be nice if we could track who is winning.

**Implement a way to track each won game (i.e.: when a game is won, track (1) what game it was, (2) who won, and (3) the date).**

There are two ways you could approach this:

- If you're also completing the node-interview exercise, you can call the API endpoints from that project and store the wins in a MongoDB database
- If you're just focusing on the Angular side of things (or you'd rather just keep them seperate), you can store the wins in an array somewhere. **BUT, you should build it as if you were sending this data to a database.**

*Hint: There might already be some functionality in the project that could help you here*

## 3. (BONUS) Ideas for Extra Features

1. Expand Rock-Paper-Scissors to [Rock-Paper-Scissors-Lizard-Spock](https://youtu.be/cSLeBKT7-sM)
2. Add imagery to break up the monotony of text
3. Allow the user to enter their name (instead of just being dubbed the "Human" player)
4. An admin dashboard view for showing win data
5. Expand our arcade by building another game!

Feel free to get creative!

## 4. Submission

You're all done!

To submit your solution, send us an [email](mailto:arcade-interview@cisco.com) with the following:

### Your Name

... so we know who you are.

### A Link to Your Code

Create a repository on a service like Github or Bitbucket and provide the link.

**Please don't commit back into this repository! It will be publically accessible and we don't want people stealing your solution!**

### Code Improvements

If you had more time, is there anything you'd do differently (in your code OR in OURS)? What would you do and why?

### Reflection & Feedback

What did you think about this exercise? Did you like it? Did you hate it? Were the instructions clear? Was is too easy? Too hard?